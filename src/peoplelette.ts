import people from "./people.json";

import { Carousel } from "./carousel";
import { getKebabCase } from "./utils/getKebahCase.util";

const PEOPLELETA_ELEMENT_PREFIX = "ppl";

const PEOPLELETA_APP_NAME = "peopleleta";

const PEOPLELETA_ELEMENT_ID = `${PEOPLELETA_ELEMENT_PREFIX}-${PEOPLELETA_APP_NAME}`;

const identifyedPeople = people.map((p, index) => ({
  id: `${index}-${getKebabCase(p.name)}`,
  name: p.name,
  imageSrc: p.imageSrc,
}));

const carousel = new Carousel(
  identifyedPeople,
  PEOPLELETA_ELEMENT_ID,
  PEOPLELETA_ELEMENT_PREFIX,
);

class Peoplelette {
  active: number;

  carousel: Carousel;

  constructor(carousel: Carousel) {
    this.active = 0;
    this.carousel = carousel;
  }

  start(): void {
    this.carousel.render();
  }
}

const peoplelette = new Peoplelette(carousel);

peoplelette.start();
