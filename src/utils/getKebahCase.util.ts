export const getKebabCase = (
    noKebabCaseString: string,
): string => {
    return noKebabCaseString
        .replace(/([a-z])([A-Z])/g, "$1-$2")
        .replace(/[\s_]+/g, "-")
        .toLowerCase();
};
