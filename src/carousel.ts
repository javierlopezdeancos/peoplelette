import { ElementFlags } from "typescript";
import { getKebabCase } from "./utils/getKebahCase.util";

const enum Horizontal {
  left = "left",
  right = "right",
}

type Position = {
  newPos: number;
  startPos: number;
};

type Person = {
  id: string;
  name: string;
  imageSrc: string;
};

type PeopleIdsOut = {
  left: string[];
  right: string[];
};

export class Carousel {
  people: Person[];

  peopleIdsOut: PeopleIdsOut;

  rootElement: HTMLElement | null;

  carouselElementId: string;

  carouselElement: HTMLElement | undefined;

  elementPrefix: string | undefined;

  observer: IntersectionObserver | undefined;

  position: Position;

  errors: Record<string, string>;

  movementDirection: Horizontal | undefined;

  elementDataAttributePrefix: string;

  constructor(
    people: Person[],
    rootElementId: string,
    elementPrefix = "",
  ) {
    this.people = people;
    this.elementPrefix = elementPrefix;
    this.carouselElementId = `${elementPrefix}-carousel`;
    this.elementDataAttributePrefix = "data-person";

    this.rootElement =
      document.getElementById(rootElementId);

    this.peopleIdsOut = {
      left: [],
      right: [],
    };

    this.position = {
      newPos: 0,
      startPos: 0,
    };

    this.errors = {
      NO_CAROUSEL_ELEMENT: "No carousel element in the dom",
      NO_ROOT_ELEMENT: "No root element to render carousel",
      NO_PEOPLE_ELEMENT: "No people elements in the dom",
      NO_OBSERVER:
        "No intersection observer available to watch",
      NOT_ALLOWED_MOVEMENT:
        "The node movement is not allowed",
      NO_PEOPLE_TO_MOVE:
        "No people elements out of viewport to move",
    };
  }

  private getPeopleElements():
    | NodeListOf<Element>
    | undefined {
    return document.querySelectorAll(
      `.${this.carouselElementId}-person`,
    );
  }

  private getLastPersonElementAtRight():
    | Element
    | undefined {
    const peopleElements = this.getPeopleElements();

    if (!peopleElements) {
      return;
    }

    return peopleElements[peopleElements.length - 1];
  }

  private isEntryOutFromLeft(
    entry: IntersectionObserverEntry,
  ): boolean {
    return (
      entry.intersectionRatio === 0 &&
      entry.boundingClientRect.x < 0
    );
  }

  private isEntryOutFromRight(
    entry: IntersectionObserverEntry,
  ): boolean {
    return (
      entry.intersectionRatio === 0 &&
      entry.boundingClientRect.x > 0
    );
  }

  private handleIntersection(
    entries: IntersectionObserverEntry[],
    observer: IntersectionObserver,
  ): void {
    entries.forEach((entry: IntersectionObserverEntry) => {
      const entryElementId = entry?.target?.id;
      const isOutFromRight =
        this.isEntryOutFromRight(entry);

      const isOutFromLeft = this.isEntryOutFromLeft(entry);
      const entryElementIdIsInPeopleIdsOutRight =
        this.peopleIdsOut.right.includes(entryElementId);
      const entryElementIdIsInPeopleIdsOutLeft =
        this.peopleIdsOut.left.includes(entryElementId);

      if (
        isOutFromRight &&
        !entryElementIdIsInPeopleIdsOutRight
      ) {
        this.addPersonElementId(
          entryElementId,
          Horizontal.right,
        );
      } else if (
        isOutFromLeft &&
        !entryElementIdIsInPeopleIdsOutLeft
      ) {
        this.addPersonElementId(
          entryElementId,
          Horizontal.left,
        );
      }

      console.log(
        "peopleIdsOut in intersection",
        this.peopleIdsOut,
      );
    });
  }

  private getPeopleElementsByIds(
    ids: string[],
  ): Element[] | null {
    let elements: Element[] = [];

    ids.forEach((id) => {
      const element = document.getElementById(`${id}`);

      if (element) {
        elements.push(element);
      }
    });

    return elements?.length > 0 ? elements : null;
  }

  private addPersonElementId(
    id: string,
    where: Horizontal,
  ): void {
    if (where === Horizontal.left) {
      const isIdOutLeft =
        this.peopleIdsOut?.left?.includes(id);

      if (!isIdOutLeft) {
        this.peopleIdsOut.left.push(id);
      }
    } else if (where === Horizontal.right) {
      const isIdOutRight =
        this.peopleIdsOut?.right?.includes(id);

      if (!isIdOutRight) {
        this.peopleIdsOut.right.push(id);
      }
    }
  }

  private removePersonElementId(
    id: string,
    where: Horizontal,
  ): void {
    if (where === Horizontal.right) {
      const isIdOutLeft =
        this.peopleIdsOut?.left?.includes(id);

      if (isIdOutLeft) {
        const ids = [...this.peopleIdsOut.left].filter(
          (i) => i !== id,
        );

        if (ids) {
          this.peopleIdsOut.left = ids;
        }
      }
    } else if (where === Horizontal.left) {
      const isIdOutRight =
        this.peopleIdsOut?.right?.includes(id);

      if (isIdOutRight) {
        const ids = [...this.peopleIdsOut.right].filter(
          (i) => i !== id,
        );

        if (ids) {
          this.peopleIdsOut.right = ids;
        }
      }
    }
  }

  private movePersonElement(
    personElementToMove: Element,
    personElementToHook: Element,
    where: Horizontal,
  ): void {
    const personIdToMove =
      personElementToMove.getAttribute("id");

    if (!personIdToMove) {
      console.error(this.errors.NO_PEOPLE_TO_MOVE);
      return;
    }

    if (!personElementToMove?.parentNode) {
      console.error(this.errors.NO_CAROUSEL_ELEMENT);
      return;
    }

    if (where === Horizontal.right) {
      personElementToMove?.parentNode.insertBefore(
        personElementToMove,
        personElementToHook.nextSibling,
      );

      this.removePersonElementId(
        personElementToMove.id,
        Horizontal.right,
      );

      this.addPersonElementId(
        personElementToMove.id,
        Horizontal.left,
      );
    } else if (where === Horizontal.left) {
      personElementToMove?.parentNode.insertBefore(
        personElementToMove,
        personElementToHook.previousSibling,
      );

      this.removePersonElementId(
        personElementToMove.id,
        Horizontal.left,
      );

      this.addPersonElementId(
        personElementToMove.id,
        Horizontal.right,
      );
    }

    console.log(this.peopleIdsOut);
  }

  private resetCarouselEventsAndFlagsAfterMovement() {
    this.addCarouselObserver();
  }

  private moveOutPeopleElements(): void {
    if (this.movementDirection === Horizontal.left) {
      const outLeftPeopleElements =
        this.getPeopleElementsByIds(this.peopleIdsOut.left);

      if (!outLeftPeopleElements) {
        console.error(this.errors.NO_PEOPLE_TO_MOVE);
        return;
      }

      const firstOutLeftPersonElement =
        outLeftPeopleElements[0];

      if (!firstOutLeftPersonElement) {
        console.error(this.errors.NO_CAROUSEL_ELEMENT);
        return;
      }

      const lastPersonElementAtRight =
        this.getLastPersonElementAtRight();

      if (!lastPersonElementAtRight) {
        console.error(this.errors.NOT_ALLOWED_MOVEMENT);
        return;
      }

      this.movePersonElement(
        firstOutLeftPersonElement,
        lastPersonElementAtRight,
        Horizontal.right,
      );

      this.resetCarouselEventsAndFlagsAfterMovement();
    }
  }

  private addMouseEventListeners(): void {
    const resetMoveDirection = (): void => {
      this.movementDirection = undefined;
    };

    const handleMouseMove = async (
      e: any,
    ): Promise<void> => {
      if (!this.carouselElement) {
        console.error(this.errors.NO_CAROUSEL_ELEMENT);
        return;
      }

      // calculate the new position
      this.position.newPos =
        this.position.startPos - e.clientX;

      this.movementDirection =
        this.position.startPos - e.clientX > 0
          ? Horizontal.left
          : Horizontal.right;

      // with each move we also want to update the start X
      this.position.startPos = e.clientX;

      // set the element's new position:
      this.carouselElement.style.left =
        this.carouselElement.offsetLeft -
        this.position.newPos +
        "px";

      this.moveOutPeopleElements();
    };

    const handleMouseDown = (e: any) => {
      e.preventDefault();

      if (!this.carouselElement) {
        console.error(this.errors.NO_CAROUSEL_ELEMENT);
        return;
      }

      // get the starting position of the cursor
      this.position.startPos = e.clientX;

      this.carouselElement.addEventListener(
        "mousemove",
        handleMouseMove,
        false,
      );
    };

    const removeMouseMoveEventListener = (): void => {
      if (!this.carouselElement) {
        console.error(this.errors.NO_CAROUSEL_ELEMENT);
        return;
      }

      resetMoveDirection();

      this.carouselElement.removeEventListener(
        "mousemove",
        handleMouseMove,
        false,
      );
    };

    const addMouseDownEventListener = (): void => {
      if (!this.carouselElement) {
        console.error(this.errors.NO_CAROUSEL_ELEMENT);
        return;
      }

      this.carouselElement.addEventListener(
        "mousedown",
        handleMouseDown,
        false,
      );
    };

    const addMouseUpEventListener = (): void => {
      if (!this.carouselElement) {
        console.error(this.errors.NO_CAROUSEL_ELEMENT);
        return;
      }

      this.carouselElement.addEventListener(
        "mouseup",
        removeMouseMoveEventListener,
        false,
      );
    };

    addMouseDownEventListener();
    addMouseUpEventListener();
  }

  private addPersonObserver(personElement: Element): void {
    if (!this.observer) {
      console.error(this.errors.NO_OBSERVER);
      return;
    }

    this.observer.observe(personElement);
  }

  private addCarouselObserver(): void {
    if (!this.rootElement) {
      console.error(this.errors.NO_ROOT_ELEMENT);
      return;
    }

    let options = {
      root: this.rootElement,
      rootMargin: "0px",
      threshold: 1.0,
    };

    this.observer = new IntersectionObserver(
      this.handleIntersection.bind(this),
      options,
    );

    const peopleElements = this.getPeopleElements();

    if (!peopleElements) {
      console.error(this.errors.NO_PEOPLE_ELEMENT);
      return;
    }

    peopleElements.forEach((personElement) => {
      this.addPersonObserver(personElement);
    });
  }

  private renderCarouselElement(): void {
    if (!this.rootElement) {
      console.error(this.errors.NO_ROOT_ELEMENT);
      return;
    }

    const carouselElement = document.createElement("div");

    carouselElement.setAttribute(
      "id",
      this.carouselElementId,
    );

    carouselElement.className = this.carouselElementId;

    this.carouselElement = carouselElement;
    this.rootElement.append(carouselElement);
  }

  private renderPersonElement(person: Person): void {
    if (!this.carouselElement) {
      console.error(this.errors.NO_CAROUSEL_ELEMENT);
      return;
    }

    const kebabCaseName = getKebabCase(person.name);
    const personElement = document.createElement("img");

    personElement.setAttribute(
      `${this.elementDataAttributePrefix}-name`,
      kebabCaseName,
    );

    personElement.setAttribute("src", person.imageSrc);
    personElement.setAttribute("id", `${person.id}`);
    personElement.className = `${this.carouselElementId}-person  ${this.carouselElementId}-person-${kebabCaseName}`;

    this.carouselElement.append(personElement);
  }

  private renderPeopleElements(): void {
    for (let p = 0; p < this.people.length; p++) {
      const person = this.people[p];

      if (!person) {
        continue;
      }

      this.renderPersonElement(person);
    }
  }

  public render(): void {
    this.renderCarouselElement();
    this.renderPeopleElements();
    this.addCarouselObserver();
    this.addMouseEventListeners();
  }
}
